package home.test.gmail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

public class GmailTest {
    private static WebDriver driver;

    @Before
    public void firstRun() {
        System.setProperty("webdriver.chome.driver", "../chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        String url = "https://mail.google.com";
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void enterGmail() {
        WebElement email_phone = driver.findElement(By.xpath("//input[@id='identifierId']"));
        email_phone.sendKeys("babaanya1974@gmail.com");
        driver.findElement(By.id("identifierNext")).click();
        WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(password));
        password.sendKeys("88wdxterqw");
        // driver.findElement(By.id("passwordNext")).click();
        driver.findElement(By.xpath("//span[@class='RveJvd snByac']")).click();
        password.submit();
        driver.findElement(By.xpath(".//div[text()='Compose']")).click();
        //WebElement message = driver.findElement(By.xpath("//textarea[@id=':oq']"));
        driver.findElement(By.name("to")).sendKeys("sofdatc@gmail.com");
        driver.findElement(By.className("aoT")).sendKeys("My Test ");
        driver.findElement(By.className("aoO")).click();
        //driver.findElement(By.cssSelector(".T-I J-J5-Ji aoO T-I-atl L3 T-I-Zf-aw2"));
        //
    }

    @After
    public void runAfter() throws Exception {
        driver.quit();
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;

        }

    }
}

